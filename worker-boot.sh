#!/bin/bash 

echo "################### Giovanni Ciatto's initialisation script ####################"

echo "Shared volume mount directory: $MNT_DIR"
echo "Master address in subnet: $MASTER_ADDR"

yum -y -q  install nfs-utils nfs-utils-lib java-1.8.0-openjdk java-1.8.0-openjdk-devel unzip git && echo "SW correclty installed" || echo "Problem while installing SW"

chkconfig rpcbind on && echo "Rpcbind service correctly enabled" || echo "Problem while enabling rpcbind service"
service rpcbind start && echo "Rpcbind service correctly started" || echo "Problem while starting rpcbind service"
chkconfig nfslock on && echo "Nfslock service correctly enabled" || echo "Problem while enabling nfslock service"
service nfslock start && echo "Nfslock service correctly started" || echo "Problem while starting nfslock service"

mkdir $MNT_DIR && echo "Created directory $MNT_DIR for mounting shared dev" || echo "Problem while creating $MNT_DIR"
echo "$MASTER_ADDR:$MNT_DIR $MNT_DIR nfs rw,sync,hard,intr 0 0" >> /etc/fstab
echo "Here is the /etc/fstab file:"
cat /etc/fstab

mount $MASTER_ADDR:$MNT_DIR $MNT_DIR && echo "$MASTER_ADDR:$MNT_DIR successfully mounted to $MNT_DIR" || echo "Problem while mounting $MASTER_ADDR:$MNT_DIR"

chmod 777 $MNT_DIR

echo "$MASTER_ADDR master-node" >> /etc/hosts

cd /etc/yum.repos.d
wget https://research.cs.wisc.edu/htcondor/yum/RPM-GPG-KEY-HTCondor
rpm --import RPM-GPG-KEY-HTCondor
wget https://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-stable-rhel6.repo

yum -y -q install condor-all

CONDOR_CONFIG_FILE=/etc/condor/condor_config
echo "CONDOR_HOST = $MASTER_ADDR" >> $CONDOR_CONFIG_FILE
echo 'FULL_HOSTNAME = $(IP_ADDRESS)/$(HOSTNAME)' >> $CONDOR_CONFIG_FILE
echo "DAEMON_LIST = MASTER, SCHEDD, STARTD" >> $CONDOR_CONFIG_FILE
echo "NETWORK_INTERFACE = 192.168.1.*" >> $CONDOR_CONFIG_FILE
echo 'ALLOW_WRITE = $(ALLOW_WRITE), $(NETWORK_INTERFACE)' >> $CONDOR_CONFIG_FILE
echo "UID_DOMAIN = 192.168.1.*" >> $CONDOR_CONFIG_FILE
echo "TRUST_UID_DOMAIN = True" >> $CONDOR_CONFIG_FILE
echo "This is the $CONDOR_CONFIG_FILE file"
cat $CONDOR_CONFIG_FILE

service condor start
condor_reconfig

echo "################ End of Giovanni Ciatto's initialisation script ################"
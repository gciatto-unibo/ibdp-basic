Infrastructures for Big Data Processing - Basic
===============================================

[Giovanni Ciatto](mailto:giovanni.ciatto@unibo.it)'s final relation

## Exam 1) Building an infrastructure on top of some IaaS Cloud Provider

<!-- Si costruisca una piccola infrastruttura basata su Cloud tramite template. -->
The student must instantiate a simple Cloud __infrastructure__ through __templates__.

<!-- Su questa infrastruttura dovrà essere installato e configurato un batch system che gestisca almeno due Worker Node. (Master del batch system e 2WN - il master potrebbe essere configurato per essere un terzo WN). -->
A __batch system__ must be installed on top of such an infrastructure and it must be configured in such a way it can handle at least two worker nodes (E.g., __one master__ node coordinating __two worker__ nodes, and the master may serve as a worker node too).

<!-- Dovrà essere creato uno spazio storage (volume) accedibile dai WN. (hint: è possibile utilizzare NFS). -->
A __storage volume__ must be included too within the infrastructure in such a way that it can be accessed by worker nodes (Hint: __NFS__ can be used).  

<!-- Per l’infrastruttura è possibile usare il tenant OpenStack a disposizione del corso e template Heat. -->
The infrastructure can be instantiated through an __heat template__ deployed on top of the __OpenStack__ tenant shown within the course.

<!-- Come batch system è possibile utilizzare HTcondor o una qualsiasi alternativa, possibilmente opensource, i.e. torque/maui, SLURM. -->
As a batch system the student can use __HTCondor__ or any other (possibly open source) alternative such as Torque/Maui or SLURM.

<!-- Scegliere una applicazione adatta ad un calcolo di tipo batch e preparare alcuni job di test da sottomettere al batch system installato e configurato. Dati di input e di output dovranno essere gestiti sia via Sandbox che via storage via NFS. -->
The student must then choose an __application__ strongly relying on batch computation and he/she must then define some test jobs to be submitted to the batch system. Input/output data should be handled by leveraging on both the __Sandbox__ and the __shared volume__.

<!-- Oltre a realizzare l’infrastruttura si descriva quanto fatto in un documento (pdf) giustificando eventuali scelte. Si descriva inoltre in dettaglio la struttura dei job. -->
Other than realising the infrastructure, the student must __describe__ what he/she did through a __PDF__ document motivating his/her choices.
He/she must also describe the nature of the chosen job. 

### Requirements analysis

The exam requires a Cloud infrastructure to be designed and deployed to handle the possibly concurrent execution of the many batch computations required by some chosen application. 

In particular the exam gives some d.o.f. to the students in terms of free choices:
1. the cloud provider
2. the template system
3. the infrastructure details such as:
    - the number of worker nodes
    - the network topology and partitioning
    - access rights and machine roles
    - etc
4. the network file system technology
5. the batch system software
6. the test application to be run

For what concerns choice point n. 1, the students are strongly encouraged to employ the [OpenStack tenant provided by CNAF](https://horizon.cloud.cnaf.infn.it/dashboard), since it is the technology presented within the course.
For this reason, no further decision has been evalued for this point.

For what concerns choice point n. 2, choosing OpenStack as tenant in turns dictates the template system to be adopted, that is, OpenStack's [Heat](https://docs.openstack.org/heat) orchestration subsystem.

Again, choice point n. 1 restricts the d.o.f. for what concerns choice point n. 3.
For instance, students' accounts on CNAF's OpenStack tenant come with severe limitations to the resources they can exploit, such as:
- the maximum number of virtual machines a student can instantiate is 3
- the maximum number of GiBs of RAM a student may exploit is 6
- the maximum number of GiBs of storage space a student may use for shared volumes is 50
- etc

This implies, above all, that the batch system will be only able to rely on 3 worker nodes in the best scenario since the infrastructure won't be able to contain more than 3 machines.
Nevertheless, the infrastructure should be designed to be parametric in the number of (worker) nodes, in such a way that it may potentially scale up to an arbitrary number of nodes.
Given that a batch system usually requires:
- at least one machine coordinating the cluster 
- at least one machine being accessible from the outside of the LAN used to inter-connect the cluster to handle job submission requests
- at least two machines dedicated to the execution of the jobs

then there will be one machine taking care of two or more the above tasks.

For what concerns choice point n. 4 and 5, students are again encouraged to employ the technologies presented during the course, namely [Network File System (NFS)](http://nfs.sourceforge.net/nfs-howto/ar01s02.html#whatis_nfs) and [HTCondor](https://research.cs.wisc.edu/htcondor), respectively.

Finally, when it comes to choice point n. 6, there is no restriction.

## The proposed infrastructure

Here we describe our proposed infrastructure and its reification as an HEAT template.

### Design

The following figure depicts the proposed network topology:

![Network topology](./res/img/network_topology.png)

It consists of a network containing `N+1` Linux machines: one is called `master-node`, while the other ones are called `worker-<i>-node` where `<i>` is a meta-variable ranging from `1` to `N+1`.

The `master-node` node machine is a special worker node which is also in charge of listening for SSH connection coming from the Internet.
For this reason a router is introduced between the `master-node` and the `public` network, assigning a public floating IP to the `master-node`.

If no limitation on the amount of machines were given, it would have been a good idea to avoid exposing the `master-node` to the Internet, maybe moving such a responsability into some bastion machine.

Since we need all the three machines to satisfy the exam requirements, we decided to simply make the `master-node` publicly available and to ensure it through protection domains.

In particular, we want the `master-node` to only be able to receive incoming traffic over just a few well-known standard protocols such as SSH, HTTP(S) and DNS (and their well-known ports), while strictly forbidding any other sort of incoming traffic.
The reason for these protocols to remain enabled is simple:
- SSH is needed to let us access the infrastructure and submit jobs to the cluster
- HTTP(S) is needed to let us download software and application data through the Web
- DNS is needed to resolve addresses

At the same time, we also want nodes to be able to communicate without restrictions on their private LAN.
The reason to do so is simple: we want to keep cluster administration simple.
Apparently, NFS and HTCondor leverages on several dynamically chosen ports by default, which may easily conflict with firewall configurations, since the latter require ports to be known _a-priori_.
At the same time, configuring NFS and HTCondor to only employ a static set of ports may lead to cumbersome configuration overhead and firewall rules.
For these reasons, we let `master-node` and the `worker-<i>-node`s communicate over an unrestricted sub-network, like for instance `192.168.1.*`.

Even if it is not depicted in the figure above, we chose to connect a 10 GiB volume to the `master-node`, making it available to the other nodes through NFS.
In any case, the volume is accessible at every node through the `/media/shared/` mount point. 

Because of this configuration, `master-node` plays the role of a NFS server, while the other worker nodes are NFS clients.
This is of interest because the two roles require slightly different configurations and, in particular, the server IP must be known when the clients are configures.
For this reason, we need to force the deployment of `master-node` _before_ the other nodes, and we also need to force `master-node` to be assigned to a _predictable_ IP address, like for instance `192.168.1.10`, while other nodes can be assigned to any IP, in principle.

As shown in the figure below, when it comes to HTCondor, machines can be play different roles, depending on the daemons they have spawned:
- Execute-only machines can only execute jobs coming from other machines within the cluster;
- Submit-only machines simply act as interfaces, in the sense that they can be used by users to submit jobs, delegating their execution other machines;
- Regular nodes machines can both execute jobs and they can be used for job submission too
- The central manager machine acts like a regular machine but it is also in charge of coordinating the cluster

![Typical HTCondor pool](./res/img/typical_condor_pool.png)

In our proposed infrastructure, the `master-node` acts as a central manager, while the other worker nodes can be either regular machines or execute-only machines.
Again, to simply configurations, we require the IP of the `master-node` to be static or predictable.

### The Heat template(s)

The proposed infrastructure is reified as a Heat template split into 4 files:
- [`ifbdp-mw.yaml`](./ifbdp-mw.yaml) is the root template describing how the whole infrastructure is composed
- [`worker-node.yaml`](./worker-node.yaml) is a parametric template defining the generic worker node and its connection to the private sub-network
- [`master-boot.sh`](./master-boot.sh) is the initialisation script for the `master-node`
- [`worker-boot.sh`](./worker-boot.sh) is the initialisation script for all `worker-<i>-node`s

### The root template ([`ifbdp-mw.yaml`](./ifbdp-mw.yaml))

The root template represents the proposed infrastructure.
It requires the user to (explicitly or implicitly) provide a number of parameters regulating its structure and composition:
- `keyname` is the ID of the public key which will be enabled to access to the infrastructure's machines.
Its value is assumed to be the identifier of an already-registered key on the OpenStack tenant.
It has no default since it is highly user specific
- `image` is the ID of the virtual machine image to be loaded on the virtual machines. 
Its value is assumed to be the identifier of an already-registered image on the OpenStack tenant.
It defaults to the `centos-6.8-x86_64-cnaf` image.
- `flavor` is the ID of the OpenStack flavour to be applied to the virtual machines, defining their compute, memory, and storage capacities.
Its value is assumed to be the identifier of an already-registered flavour on the OpenStack tenant.
It defaults to `d1.small` meaning that each machine will be equipped with 1 CPU, 1 GiB of RAM and 10 GiB of storage.
- `floating-network-id` is the ID of the public network the master node will be connected to.
Its value is assumed to be the identifier of an already-registered network on the OpenStack tenant.
It defaults to `public` which is the only available public network within the provided OpenStack tenant.
- `shared-volume-size` is the amount of GiB of the shared volume.
It is an integer number and it defaults to 10 GiB
- `n-workers` is the amount of worker nodes (other than the `master-node`) to be deployed.
It is an integer number and it defaults to 2
- `shared-volume-device` is the path (within the `master-node`'s `/dev` directory) for the shared volume.
It is a string and it defaults to `/dev/vdb`
- `shared-volume-mountfolder` is the mount path for the shared volume on both the `master-node` and all `worker-<i>-node`s.
It is a string and it defaults to `/media/shared`
- `subnet-cidr` is the subnet id for the workers sub-network, in CIDR notation.
It is a string and it defaults to `192.168.1.0/24`, meaning that virtual machines may be assigned to IPs ranging from `192.168.1.1` to `192.168.1.255`
- `subnet-gateway-ip` is the subnet gateway's IP.
It is a string and it defaults to `192.168.1.1`
- `master-node-address` is the desired IP address of the `master-node` within the private sub-network.
It is a string and it defaults to `192.168.1.10`

A number of _resources_ are then defined within the template in order to instantiate a LAN and connect it to the Internet:

1. A private network named `private_net`:

    ```yaml
    private_net:
    type: OS::Neutron::Net
    properties:
      name: private_net
    ````

0. A subnetwork of `private_net` named `private_subnet` 

    ```yaml
    private_subnet:
    type: OS::Neutron::Subnet
    properties:
      name: private_subnet
      network_id: { get_resource: private_net }
      cidr: { get_param: subnet-cidr }
      gateway_ip: { get_param: subnet-gateway-ip }
    ```

    It employs the IP range defined through the `subnet-cidr` parameter (i.e., `192.168.1.0/24`) and it forces the gateway to be assigned to the IP defined in the `subnet-gateway-ip` parameter (i.e., `192.168.1.1`)

0. A floating IP aimed at connecting the network specified by the `floating-network-id` (i.e., `public`) parameter to the Internet

    ```yaml
    floating-ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network_id: { get_param: floating-network-id }
    ```

0. A router connected to the public network specified by the `floating-network-id` (i.e., `public`) 

    ```yaml
    router1:
    type: OS::Neutron::Router
    properties:
      external_gateway_info:
        network: { get_param: floating-network-id }
    ```

0. A router interface connecting it to the `private_subnet`

    ```yaml
    my-router-interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: router1 }
      subnet_id: { get_resource: private_subnet }
    ```

Since we want to constrain the traffic entering the LAN from the Internet to what is strictly necessary, still allowing workers from within the LAN to freely communicate, we define the following protection groups:

1. `external-sec-group` only enables incoming traffic coming from the Internet for protocols ICMP, SSH (tcp 22), HTTP (tcp 80), HTTPS (tcp 443) and DNS (udp 443)

    ```yaml
    external-sec-group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: external-sec-group
      rules:
        - protocol: icmp        # ICMP
          direction: ingress

        - protocol: tcp         # SSH
          direction: ingress
          port_range_min: 22
          port_range_max: 22
          
        - protocol: tcp         # HTTP
          direction: ingress
          port_range_min: 80
          port_range_max: 80
           
        - protocol: tcp         # HTTPS
          direction: ingress
          port_range_min: 443
          port_range_max: 443

        - protocol: udp         # DNS
          direction: ingress
          port_range_min: 53
          port_range_max: 53
    ```

    Of course, outgoing traffic is enabled for all destinations and protocols, by default

0. `internal-sec-group` is needed to enable any sort of traffic between worker nodes

    ```yaml
    internal-sec-group:
    type: OS::Neutron::SecurityGroup
    properties:
      name: internal-sec-group
      rules:
        - protocol: icmp
          remote_ip_prefix: { get_param: subnet-cidr }
          direction: ingress

        - protocol: tcp                                 # All TCP ports
          remote_ip_prefix: { get_param: subnet-cidr }
          direction: ingress 
          port_range_min: 1  
          port_range_max: 65535

        - protocol: udp                                 # All UDP ports
          remote_ip_prefix: { get_param: subnet-cidr }
          direction: ingress 
          port_range_min: 1  
          port_range_max: 65535
    ```

A port to the `private_net`work to be used by the `master-node` is then defined, making it part of both the `internal` security domain and the `external` one

```yaml
master-node-port:
type: OS::Neutron::Port
properties:
  name: master-node-port
  network: { get_resource: private_net }
  security_groups:
    - get_resource: external-sec-group
    - get_resource: internal-sec-group
  fixed_ips:
    - subnet_id: { get_resource: private_subnet }
      ip_address: { get_param: master-node-address } # Forcing the master-node to be assigned to a specific IP 
```

Notice the property `ip_address: { get_param: master-node-address }`. In such a way we are forcing the `master-node` to be assigned to a specific IP within the internal sub-network, namely, the one provided by the user through the `master-node-address` parameter.
This is needed to simplify the setup for NFS and HTCondor.

The `master-node` is then defined as follows:

```yaml
master-node:
  type: OS::Nova::Server
  properties:
    name: master-node
    key_name: { get_param: keyname }
    image: { get_param: image }
    flavor: { get_param: flavor }
    networks:
      - port: { get_resource: master-node-port }
    user_data_format: SOFTWARE_CONFIG
    user_data:
    str_replace:
      template: { get_file: master-boot.sh } 
    params:
      $DEV: { get_param: shared-volume-device }
      $MNT_DIR: { get_param: shared-volume-mountfolder }
      $SUBNET: { get_param: subnet-cidr }
      $MY_ADDR: { get_param: master-node-address }
```

Notice that an initialisation script is provided as a string to the `master-node` through the `user-data` property.
The `str_replace: { template: <Origin>, params: { <Pre1>: <Post1> ... <PreN>: <PostN> } }` is an Heat construct which can be provided in place of a string value.
It is dynamically replaced by the string attained by replacing all occurrences of substrings `Pre1`, ..., `PreN` in `Origin` with `Post1`, ..., `PostN` respectively.
In particular, the `Origin` actually employed here is the whole content of the [`master-boot.sh`](./master-boot.sh) file, where variables `$DEV`, `$MNT_DIR`, `$SUBNET`, `$MY_ADDR` are replaced by the values of the Heat parameters `shared-volume-device`, `shared-volume-mountfolder`, `subnet-cidr`, `master-node-address`.
Such an initialisation script is in charge of configuring the `master-node` to act as server for NFS and as coordinator for the HTCondor cluster.
Its fuctioning is described in detail later in this document.

The floating IP defined above is then assigned to the `master-node`, associating it to the `master-node-port`:

```yaml
floating-ip-association:
  type: OS::Neutron::FloatingIPAssociation
  properties:
    floatingip_id: { get_resource: floating-ip }
    port_id: { get_resource: master-node-port }
```

To instantiate a shared volume and to attach it to the `master-node`, the following steps are then required:

1. A shared volume is defined having the size specified by the user through the `shared-volume-size` parameter

    ```yaml
    shared-volume:
    type: OS::Cinder::Volume
    properties:
      size: { get_param: shared-volume-size }
      availability_zone: nova
    ```

0. The shared volume is then attached to the `master-node`

    ```yaml
    volume-attachment-to-master-node:
    type: OS::Cinder::VolumeAttachment
    properties:
      volume_id: { get_resource: shared-volume }
      instance_uuid: { get_resource: master-node }
      mountpoint: { get_param: shared-volume-device }
    ```

    Within the `master-node`, the volume will be available as a device file to the path defined by the `shared-volume-device` parameter (i.e., ``/dev/vdb`)

Finally, a parametric amount of worker nodes, along with their ports to the private subnetwork, should be defined.
The particular details of how the `worker-<i>-node` is defined, are discussed into the following subsection describing the [`worker-node.yaml`](./worker-node.yaml) template.
Here we discuss how a given amount of templates (namely, the number specified by the `n-workers` parameter) can be instantiated throught Heat's _resource groups_:

```yaml
worker-nodes:
  type: OS::Heat::ResourceGroup
  depends_on: 
    - private_subnet
    - master-node
  properties:
    count: { get_param: n-workers}
    resource_def:
      type: worker-node.yaml
      properties:
        index: "%index%"
        keyname: { get_param: keyname }
        image: { get_param: image }
        flavor: { get_param: flavor }
        network: { get_resource: private_net }
        network-security-group: { get_resource: internal-sec-group }
        shared-volume-mountfolder: { get_param: shared-volume-mountfolder }
        master-node-address: { get_attr: [ master-node, first_address ] }
```

A resource group essentially allows us to instantiate a particular template several times by means of a single definition.
In particular the amount of instances depends on the `count` property.
The structure of each instance is specified as the argument of the `resource_def` property.
Within this the `resource_def` argument object, the `"%index%"` string is dynamically replaced by the specific instance index, according to a 0-based indexing style.
This is useful, for instance, to identify workers.
A number of informations are to be provided to the [`worker-node.yaml`](./worker-node.yaml) template in order to instantiate it, like for instance:
- the `index` of the particular instance, namely `"%index%"`
- the `keyname` of the public key which will be enabled to access to the worker machines through SSH, namely the same one used for the `master-node`
- the `image` to be used for the workers' virtual machines, namely the same one used for the `master-node`
- their `flavour` to be used for the workers' virtual machines, namely the same one used for the `master-node`
- the identifier of the private `network` worker machines should be connected to, namely `private_net`
- the security group worker machines should be part of, namely `internal-sec-group`
- the `shared-volume-mountfolder`, i.e. the path on their local file system where the shared volume should be mounted on, namely the same path used for the `master-node`
- the `master-node-address`, i.e. address on the `private_net` they will assume the `master-node` will assigned to, namely the `master-node` first address (i.e., the one we assigned when defining the `master-node-port`)

### The workers' template ([`worker-node.yaml`](./worker-node.yaml))

The worker template instantiates a single worker machine along with its port to a network.
It requires the user to (explicitly or implicitly) provide a number of parameters regulating its structure and composition:
- `keyname` is the ID of the public key which will be enabled to access to the worker machine.
Its value is assumed to be the identifier of an already-registered key on the OpenStack tenant.
It has no default since it is highly user specific
- `image` is the ID of the virtual machine image to be loaded on the worker machine. 
Its value is assumed to be the identifier of an already-registered image on the OpenStack tenant.
It defaults to the `centos-6.8-x86_64-cnaf` image
- `flavor` is the ID of the OpenStack flavour to be applyied to the worker machine, defining its compute, memory, and storage capacities.
Its value is assumed to be the identifier of an already-registered flavour on the OpenStack tenant.
It defaults to `d1.small` meaning that the machine will be equipped with 1 CPU, 1 GiB of RAM and 10 GiB of storage.
- `network` is the name of the network where the `master-node` and the other machines are assumed to be available.
It is a string and it defaults to `private_net`
- `network-security-group` is the ID of the security group the worker machine should be part of.
It is a string and it defaults to `default` (no restriction)
- `shared-volume-mountfolder` is the mount path for the shared volume on the worker machine local file system
It is a string and it defaults to `/media/shared`
- `master-node-address` is the desired IP address of the `master-node` within the aforementioned network.
It is a string and has no default

The template simply instantiates two resources:

1. A port towards the network specified through the `network` parameter, to be used by the worker node machine:

    ```yaml
    resources:
      worker-node-port:
        type: OS::Neutron::Port
          properties:
            name: 
              str_replace:
                template: worker-INDEX-node-port
                params:
                  INDEX: { get_param: index }
            network: { get_param: network }
            security_groups: [{ get_param: network-security-group }]
    ```

    Notice that the port adheres to the security group provided throught the `network-security-group` parameter.
    Also notice that the name of the port resource will be `worker-<index>-node-port` where `<index>` is the value of the `index` parameter

0. A worker machine is then defined as follows:

    ```yaml
    worker-node:
      type: OS::Nova::Server
      properties:
        key_name: { get_param: keyname }
        name: 
          str_replace:
            template: worker-INDEX-node
            params:
              INDEX: { get_param: index }
        image: { get_param: image }
        flavor: { get_param: flavor }
        networks:
          - port: { get_resource: worker-node-port }
        user_data_format: SOFTWARE_CONFIG
        user_data:
          str_replace:
            template: { get_file: worker-boot.sh } 
            params:
              $MNT_DIR: { get_param: shared-volume-mountfolder }
              $MASTER_ADDR: { get_param: master-node-address }
    ```

    Notice that the name of the machine resource will be `worker-<index>-node` where `<index>` is the value of the `index` parameter.
    Also notice that, similarly to the `master-node`, a worker node will be endowed with an initialisation script, namely the [`worker-boot.sh`](./worker-boot.sh) one where the `$MNT_DIR` and `$MASTER_ADDR` variables have been replaced by the values of the `shared-volume-mountfolder` and `master-node-address` respectively.
    Such an initialisation script is in charge of configuring the `worker-<index>-node` to act as client for NFS and as a regular node for the HTCondor cluster.
    Its fuctioning is described in detail later in this document.

### The `master-node` configuration script [`master-boot.sh`](./master-boot.sh)

The master node configuration script is run by the `master-node` machine on the very first startu after deployment.
It consist of a `.sh` source to be executed by `bash`.
The script runs with `root` privileges and it is aimed at:
* installing the software libraries and runtimes required by 
    - NFS, 
    - HTCondor and 
    - the chosen final application to be executed over HTCondor
* configuring the machine to act as a NFS server
* configuring the machine to act as an HTCondor central manager

#### Installing the required software

Installing the required software is as simple as executing the following line:

```bash
yum -y -q install nfs-utils nfs-utils-lib java-1.8.0-openjdk java-1.8.0-openjdk-devel git && echo "SW correclty installed" || echo "Problem while installing SW"
```

This assumes the machine is running a `CentOS 6` operating system, where `yum` is the default package manager.
This also assumes the machine to have access to the Internet.
The `-y` (for "yes") prevents the script from asking confirmations to the human user, assuming the "yes" answer to any question.
The `-q` (for "quiet") prevents the package manager from being verbose, thus keeping the log easy to read.
The syntax `<command> || echo <message>` handles the case where an error occurs while executing the command on the left by prompting a pleasant error message.

The software packages installed by this command are:
- `nfs-utils` and `nfs-utils-lib` which are required by both NFS clients and servers
- `java-1.8.0-openjdk` `java-1.8.0-openjdk-devel`, i.e. the JRE and JDK, both for the Java 8 version, which are required by the final application
- `git` which is useful to download the final application

Notice that no package is installed concerning HTCondor.
This is because the HTCondor version available on `CentOS 6`'s repositories is outdated.
Thus we chose to install HTCondor from its main repository, namely http://research.cs.wisc.edu/htcondor/yum, following the instruction presented [here](http://research.cs.wisc.edu/htcondor/manual/v8.6/3_2Installation_Start.html#SECTION00422200000000000000).
To do so we firstly need to add the HTCondor stable repository to the local `yum` configuration:

```bash
cd /etc/yum.repos.d

# Download the repository public key
wget https://research.cs.wisc.edu/htcondor/yum/RPM-GPG-KEY-HTCondor
# Import the key
rpm --import RPM-GPG-KEY-HTCondor

# Download the repository data into /etc/yum.repos.d
wget https://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-stable-rhel6.repo

# Install HTCondor and its dependencies
yum -y -q install condor-all
```
#### Formatting and mounting the shared volume

The next step consists into formatting the shared volume and mounting it to its mount point, namely `$MNT_DIR` (i.e., `/media/shared`), assuming it is initially available as the `$DEV` device (i.e., `/dev/vdb`).
This step must be performed in order to actually share the volume through NFS.
This is achieved by means of the following commands:

```bash
# Format the device with the ext4 filesystem
mkfs.ext4 $DEV && echo "$DEV formatted with ext4 file-system" || echo "Problem while formatting $DEV"

# Create the $MNT_DIR directory
mkdir $MNT_DIR && echo "Created directory $MNT_DIR for mounting $DEV" || echo "Problem while creating $MNT_DIR"

# Add the "$DEV $MNT_DIR ext4 user,rw,auto 0 0" line to the /etc/fstab file, thus making the volume automatically mounted on future startups
echo "$DEV $MNT_DIR ext4 user,rw,auto 0 0" >> /etc/fstab

# Actually mounts $DEV to the $MNT_DIR directory
mount $DEV && echo "$DEV successfully mounted to $MNT_DIR" || echo "Problem while mounting $DEV"

# Gives free access to the $MNT_DIR to everybody
chmod 777 $MNT_DIR
```

Notice that `$DEV` and `$MNT_DIR` are not bash variable, since they are replaced by some actual values (i.e., `/dev/vdb` and `/media/shared`, respectively) by the Heat engine, thanks to the `str_replace` construct.
The syntax `<command> && echo <message_ok> || echo <message_fail>` handles the case where an error occurs while executing the command on the left by prompting a pleasant error message. If the command executes correctly then another success message is prompted instead.

#### Configuring the NFS server

In order to actually configure the `master-node` as a NFS server, the following commands are then required.
They properly enable, configure, and start the `rpcbind` and `nfs` services:

```bash
# Enables the rpcbind service
chkconfig rpcbind on && echo "RPCbind service correctly enabled" || echo "Problepinm while enabling the rpcbind service"

# Enables the nfs service
chkconfig nfs on && echo "NFS service correctly enabled" || echo "Problem while enabling the NFS service"

# Marks the $MNT_DIR directory as exported to all NFS clients from within the $SUBNET subnetwork, by appending the following line to the /etc/exports file
echo "$MNT_DIR $SUBNET(rw,sync,crossmnt,no_root_squash,no_subtree_check)" >> /etc/exports

# Reload the /etc/exports file
exportfs -a && echo "Correctly exported all shared file systems" || echo "Problem while exporting all shared file systems"

# Start the rpcbind service
rpcbind && echo "RPCbind service correctly started" || echo "Problem while starting the rpcbind service"

# Start the nfs service
service nfs start && echo "NFS service correctly started" || echo "Problem while starting the NFS service"
```

Again, notice that `$SUBNET` is not a bash variable, since it is replaced by its actual values (i.e., `192.168.1.0/24`) by the Heat engine, thanks to the `str_replace` construct.

#### Configuring the HTCondor node as a central manager

As a last step, the HTCondor services must be properly configured and started. 
To do so, the following commands need to be executed in order to add some lines to the `/etc/condor/condor_config` file:

```bash
CONDOR_CONFIG_FILE=/etc/condor/condor_config
# The condor central manager is the machine identified by the $MY_ADDR address
echo "CONDOR_HOST = $MY_ADDR"                             >> $CONDOR_CONFIG_FILE
echo 'FULL_HOSTNAME = $(IP_ADDRESS)/$(HOSTNAME)'          >> $CONDOR_CONFIG_FILE

# Other HTCondor node within this subnetwork will be enabled to join the cluster
echo "NETWORK_INTERFACE = 192.168.1.*"                    >> $CONDOR_CONFIG_FILE 
echo 'ALLOW_WRITE = $(ALLOW_WRITE), $(NETWORK_INTERFACE)' >> $CONDOR_CONFIG_FILE

# These lines are needed to let HTCondor users submit jobs from their own usernames (instead of forcing them to submit through `condor` o `root`)
echo "UID_DOMAIN = 192.168.1.*"                           >> $CONDOR_CONFIG_FILE
echo "TRUST_UID_DOMAIN = True"                            >> $CONDOR_CONFIG_FILE
```

Here we assume the `/etc/condor/condor_config` file already contains a line in the form

```
DAEMON_LIST = MASTER, SCHEDD, STARTD, NEGOTIATOR, COLLECTOR
```

which essentially states that the machine is a cental manager, since the `negotiator` and `collector` daemons are needed to coordinate the HTCondor cluster, while the `schedd` daemon is needed to let users submit jobs, the `startd` daemon is needed to let nodes execute jobs, and the `master` daemon is always needed.

Finally, the condor services can be started by means of the following commands:

```bash
service condor start

condor_reconfig
```

### The `worker-<i>-node` configuration script [`worker-boot.sh`](./worker-boot.sh)

The [`worker-boot.sh`](./worker-boot.sh) initialisation script is essentially equals to the [`master-boot.sh`](./master-boot.sh) one, except for the NFS and HTCondor configuration phases which are slightly different and the formatting phase which is not needed.

#### Configuring the NFS clients

Worker machines are indeed NFS clients.
The following commands configure a worker machine as a NFS client.
They assume the software installation phase have successfully installed the same libraries presented within the `master-booth` subsection. 

```bash
# Enable the rpcbind service
chkconfig rpcbind on && echo "Rpcbind service correctly enabled" || echo "Problem while enabling rpcbind service"

# Start the rpcbind service
service rpcbind start && echo "Rpcbind service correctly started" || echo "Problem while starting rpcbind service"

# Enable the nfslock service
chkconfig nfslock on && echo "Nfslock service correctly enabled" || echo "Problem while enabling nfslock service"

# Start the nfslock service
service nfslock start && echo "Nfslock service correctly started" || echo "Problem while starting nfslock service"

# Create teh $MNT_DIR directory that will host the shared file system
mkdir $MNT_DIR && echo "Created directory $MNT_DIR for mounting shared dev" || echo "Problem while creating $MNT_DIR"

# Append the following line to the /etc/fstab file, thus making the volume exported by the $MASTER_ADDR host to its $MNT_DIR directory automatically mounted into the local $MNT_DIR directory on future startups
echo "$MASTER_ADDR:$MNT_DIR $MNT_DIR nfs rw,sync,hard,intr 0 0" >> /etc/fstab

# Actually mount the shared volume
mount $MASTER_ADDR:$MNT_DIR $MNT_DIR && echo "$MASTER_ADDR:$MNT_DIR successfully mounted to $MNT_DIR" || echo "Problem while mounting $MASTER_ADDR:$MNT_DIR"

# Make the volume accessible by all users
chmod 777 $MNT_DIR
```

Notice that `$MASTER_ADDR` and `$MNT_DIR` are not bash variables, since they are replaced by some actual values (i.e., `192.168.1.10` and `/media/shared`, respectively) by the Heat engine, thanks to the `str_replace` construct.

#### Configuring the HTCondor node as a regular node

As a last step, the HTCondor services must be properly configured and started. 
To do so, the procedure is the same as in the `master-node` case, exept for the following lines which should be appended to the `/etc/condor/condor_config` file:

```bash
CONDOR_CONFIG_FILE=/etc/condor/condor_config

# The condor central manager is the machine identified by the $MASTER_ADDR address
echo "CONDOR_HOST = $MASTER_ADDR"           >> $CONDOR_CONFIG_FILE

# Only the `master`, `schedd`, and `stard` daemons need to be spawned on regular nodes
echo 'DAEMON_LIST = MASTER, SCHEDD, STARTD' >> $CONDOR_CONFIG_FILE
```

## Deploying the stack

The stack deployment simply requires:

- a Python + `pip` runtime to be properly installed and configured

    ```bash
    python --version # Python x.x.x

    pip --version # pip y.y.y
    ```

- the `python-openstackclient` to `pip` package to be properly installed by running

    ```bash
    pip install python-openstackclient
    ```

- a properly configured enviroment, by means of the following environment variables

  ```bash
  export OS_AUTH_URL=https://horizon.cloud.cnaf.infn.it:5000/v3
  
  export OS_IDENTITY_API_VERSION=3

  export OS_PROJECT_ID=ff4a782c94cd47fd92b7a254fcc02efd

  export OS_REGION_NAME=regionOne

  export OS_USER_DOMAIN_NAME=Default

  export OS_USERNAME=guest01

  export OS_PASSWORD=# guest01 password here
  ```

Then, supposing you have cloned the current repository by means of the

```bash
git clone https://gitlab.com/gciatto-unibo/ibdp-basic.git
```

command, you may be able to deploy the proposed infrastructure by means of the following commands:

```bash
cd ibdp-basic

openstack stack create --template ifbdp-mw.yaml --parameter keyname=<your key here> <stack name here>
```

## The test application

The test application consists in running a number of simulations by means of the [Alchemist](https://alchemistsimulator.github.io) simulator.
The original source code and context are provided [here](https://bitbucket.org/danysk/experiment-2017-tomacs).

The [Alchemist](https://alchemistsimulator.github.io) is able to execute the same simulation multiple times, concurrently, on multi-core machines.
This is achieved, through the original code, by running:

```bash
./gradlew runtomacs
```

on a shell located within the project main directory (i.e., `experiment-2017-tomacs/`).

We edited the simulation lanching procedure in such a way that each simulation may be carried out by a single HTCondor job.
The new source code is available [here](https://bitbucket.org/gciatto_unibo/experiment-2017-tomacs).

The steps needed to run it on the proposed architecture are described in the following:

1. Open a SSH shell on the master node

    ```
    ssh -i <private key here> cloud-user@<master-node's floating ip here>
    ```

0. Clone the `git` repository https://bitbucket.org/gciatto_unibo/experiment-2017-tomacs

    ```bash
    git clone https://bitbucket.org/gciatto_unibo/experiment-2017-tomacs.git --depth 1
    ```

    Cloning the whole repository may take a while since its history contains more than 1 GiB of data.
    We actually only need the last commit and this is the purpose of the `--depth 1` argument

0. Move within the repository main directory, `experiment-2017-tomacs`, which should be present within your home. 
Here you may need to make `.sh` files executable

    ```bash
    cd experiment-2017-tomacs

    chmod a+x *.sh
    ``` 

0. Submit the `simulation_job.condor` to HTCondor

    ```bash
    condor_submit.condor
    ``` 

    This will actually spawn 4 jobs running the same simulation with 4 different random seeds.

0. Once the simulations are over, the outcomes will be available within the `/media/share/data` directory.
They consist of a number of `.txt` files, one for each simulation, describing the temporal evolution of the simulated items.

### Details on the simulation job

The structure of the `experiment-2017-tomacs` project is as follows:

```
[cloud-user@master-node experiment-2017-tomacs]$ tree
.
├── build.gradle
├── effects
│   ├── ...
├── gradlew                             <-5-
├── gradlew.bat
├── input_data.sh
├── launch_job.sh                       <-2- 
├── launch_simulation.sh                <-3-
├── simulation_job.condor               <-1-
└── src
    └── main
        ├── java
        │   ├── ...
        └── resources
            ├── tomacs.template.yml     <-4-
            ├── ...
```

The files which worth to be further described are:

1. `simulation_job.condor` which is the jobs description file

    ```
    Universe = vanilla
    Executable = launch_job.sh
    Arguments = false false $(Process)

    Error = simulation.err.$(Process).txt                                                                                        
    Output = simulation.out.$(Process).txt
    Log = simulation.log.txt

    transfer_input_files = .
    transfer_output_files = simulation.out.$(Process).txt

    queue 4 
    ```

    It essentially spawns 4 jobs, each one launching a different simulation with using the `$(Process)` value as random seed.
    Other than specifying the files that will contain the jobs' standard output/error and log files, it also specifies the jobs' input sandbox, consisting of the whole `experiment-2017-tomacs` containing the Java source/class code of the simulator and the simulation, and the configuration files they need.

2. `launch_job.sh` is the initialisation script of the sigle simulation job

    ```bash
    SHARED_DIR=/media/shared/data

    # Ensure the directory /media/shared/data exists on the shared volume
    if [[ ! -d $SHARED_DIR ]]; then
        mkdir -p $SHARED_DIR
    fi

    # Download the simulation's java dependencies and compiles them through the 
    ./gradlew assemble --console=plain --stacktrace --no-daemon

    # Call the launch_simulation.sh script actually starting the simulation
    bash launch_simulation.sh $SHARED_DIR $1 $2 $3
    ```

    Notice that, as specified by the `simulation_job.condor` file, `$1`, `$2`, and `$3` are bound to the `false`, `false`, `$(Process)` values respectively, at run-time.
    So argument `$3` carries the random seed of the simulation while `$1` and `$2` are two more parameters of the simulation which are kept constant for this test scenario

3. `launch_simulation.sh` is the actual script configuring and running a particular simulation on a given random seed

    ```bash
    DESTINATION_DIR=$1
    TIME=$2
    TIMEV=time
    SMALL=$3
    SMALLV=small
    SEED=$4
    SEEDV=seed

    RES_DIR=./src/main/resources/
    TEMPLATE=tomacs.template.yml
    PATTERN1='s/__'$TIMEV'__/'$TIME'/g'
    PATTERN2='s/__'$SMALLV'__/'$SMALL'/g'
    PATTERN3='s/__'$SEEDV'__/'$SEED'/g'

    SPECS='__'$TIMEV'_'$TIME'_'$SMALLV'_'$SMALL'_'$SEEDV'_'$SEED
    FNAME="tomacs$SPECS.yml"

    sed -e $PATTERN1 -e $PATTERN2 -e $PATTERN3 $RES_DIR$TEMPLATE > $RES_DIR$FNAME

    ./gradlew runtomacs -Pdest=$DESTINATION_DIR -Pspecs=$SPECS --console=plain --stacktrace --no-daemon
    ```

    This essentially assumes the `gradlew` script to launch the simulation in such a way that it relies on the existence of the `tomacs$SPECS.yml` file within the `experiment-2017-tomacs/src/main/resources`.
    Such a file is supposed to actually tell the simulator the values of the simultion parameters, there including the random seed.
    The file is dynamically generated by the `launch_simulation.sh` script before invoking `gradlew`.
    The generation process leverages on the `experiment-2017-tomacs/src/main/resources/tomacs.template.yml` template file where all the occurrences of the `__time__`, `__small__`, and `__seed__` substrings are replaced by the values of the `$2`, `$3`, and `$4`, respectively, by means of the `sed` command.

    To further inspect the behaviour of the `gradlew` script, the reader should have a basic understanding of the [Gradle](https://gradle.org) build system and he/she may want to look at the `build.gradle` file.

    Thanks to this configuration, the simulation outcome will be the file

    ```
    /media/shared/data/tomacs__time_$2_small_$3_seed_$4
    ```
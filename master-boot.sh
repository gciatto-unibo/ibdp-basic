#!/bin/bash

echo "################### Giovanni Ciatto's initialisation script ####################"

echo "Shared volume device: $DEV"
echo "Shared volume mount directory: $MNT_DIR"
echo "Subnet CIDS: $SUBNET"

yum -y -q install nfs-utils nfs-utils-lib java-1.8.0-openjdk java-1.8.0-openjdk-devel unzip git && echo "SW correclty installed" || echo "Problem while installing SW"

mkfs.ext4 $DEV && echo "$DEV formatted with ext4 file-system" || echo "Problem while formatting $DEV"
mkdir $MNT_DIR && echo "Created directory $MNT_DIR for mounting $DEV" || echo "Problem while creating $MNT_DIR"
echo "$DEV $MNT_DIR ext4 user,rw,auto 0 0" >> /etc/fstab
mount $DEV && echo "$DEV successfully mounted to $MNT_DIR" || echo "Problem while mounting $DEV"

chkconfig rpcbind on && echo "RPCbind service correctly enabled" || echo "Problepinm while enabling the rpcbind service"
chkconfig nfs on && echo "NFS service correctly enabled" || echo "Problem while enabling the NFS service"
echo "$MNT_DIR $SUBNET(rw,sync,crossmnt,no_root_squash,no_subtree_check)" >> /etc/exports
echo "Here is the /etc/exports file:"
cat /etc/exports
exportfs -a && echo "Correctly exported all shared file systems" || echo "Problem while exporting all shared file systems"

rpcbind && echo "RPCbind service correctly started" || echo "Problem while starting the rpcbind service"

service nfs start && echo "NFS service correctly started" || echo "Problem while starting the NFS service"

chmod 777 $MNT_DIR

echo "$MY_ADDR master-node" >> /etc/hosts

cd /etc/yum.repos.d
wget https://research.cs.wisc.edu/htcondor/yum/RPM-GPG-KEY-HTCondor
rpm --import RPM-GPG-KEY-HTCondor
wget https://research.cs.wisc.edu/htcondor/yum/repo.d/htcondor-stable-rhel6.repo

yum -y -q install condor-all

CONDOR_CONFIG_FILE=/etc/condor/condor_config
echo "CONDOR_HOST = $MY_ADDR" >> $CONDOR_CONFIG_FILE
echo 'FULL_HOSTNAME = $(IP_ADDRESS)/$(HOSTNAME)' >> $CONDOR_CONFIG_FILE
echo "NETWORK_INTERFACE = 192.168.1.*" >> $CONDOR_CONFIG_FILE
echo 'ALLOW_WRITE = $(ALLOW_WRITE), $(NETWORK_INTERFACE)' >> $CONDOR_CONFIG_FILE
echo "UID_DOMAIN = 192.168.1.*" >> $CONDOR_CONFIG_FILE
echo "TRUST_UID_DOMAIN = True" >> $CONDOR_CONFIG_FILE
echo "This is the $CONDOR_CONFIG_FILE file"
cat $CONDOR_CONFIG_FILE

service condor start
condor_reconfig

echo "################ End of Giovanni Ciatto's initialisation script ################"